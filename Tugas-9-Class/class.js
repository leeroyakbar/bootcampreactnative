class Animal {
  constructor(nama,legs,darahdingin){
    this._nama = nama
    this._legs = 4
    this._darahdingin = false
  }

  get nama(){
    return this._nama;
  }
  get legs(){
    return this._legs;
  }
  get darahdingin(){
    return this._darahdingin;
  }
}

var sheep = new Animal("Shaun");
console.log(sheep.nama) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.darahdingin) // false
