class Animal {
  constructor(nama, legs, darahdingin, yell, jump){
    this.nama = nama
    this.legs = 4
    this.darahdingin = false
  }
  yell(){
    return this.yell;
  }
  jump(){
    return this.jump;
  }
}

class Ape extends Animal{
  constructor(nama, legs, yell){
    super(nama, legs, yell)
    this.nama = nama
    this.legs = 2
    this.yell = "Auooo"
  }
  
}
class Frog extends Animal{
  constructor(nama, darahdingin, jump){
    super(nama, darahdingin, jump)
    this.nama = nama;
    this.darahdingin = true
    this.jump = "hope hope"
  }
  
}


 var sheep = new Animal("Shaun");
console.log(sheep.nama) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.darahdingin) // false

console.log()

var sungokong = new Ape("kera sakti")
console.log(sungokong.nama)
console.log(sungokong.legs) 
console.log(sungokong.darahdingin) 
console.log(sungokong.yell)
 
console.log()

var kodok = new Frog("buduk")
console.log(kodok.nama)
console.log(kodok.legs) 
console.log(kodok.darahdingin) 
console.log(kodok.jump)
