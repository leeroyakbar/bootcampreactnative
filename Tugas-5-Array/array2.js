function rangeWithStep(a,n,m){
	var arr = [];
	var temp = 0;
	if(a>n){
		for(var i=a ; i>=n ; )
		{
			arr[temp++] = i;
			i-=m;
		}
	}
	else {
		for(var i=a ; i<=n ; ){
			arr[temp++] = i;
			i+=m;
		}
	}
	return arr;
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) //