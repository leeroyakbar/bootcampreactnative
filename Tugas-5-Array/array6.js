function dataHandling2(arr)
{
	arr.splice(1,1,"Roman Alamsyah Elsharawy");
	arr.splice(2,1, "Provinsi Bandar Lampung");
	arr.splice(4,1,"SMA Internasional Metro");
	return arr;
}

//bagian splice
var arr=["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]
var arr2=dataHandling2(arr);
console.log(arr2)

//bagian split
var lahir = arr2[3].split("/");
var bulan = Number(lahir[1]);
switch(bulan){
	case 1: { console.log('Januari'); break;}
	case 2: { console.log('Februari'); break;}
	case 3: { console.log('Maret'); break;}
	case 4: { console.log( 'April'); break;}
	case 5: { console.log('Mei'); break;}
	case 6: { console.log('Juni'); break;}
	case 7: { console.log('Juli'); break;}
	case 8: { console.log('Agustus'); break;}
	case 9: { console.log('September'); break;}
	case 10: { console.log('Oktober'); break;}
	case 11: { console.log('November'); break;}
	case 12: { console.log('Desember'); break;}
}
//console.log(bulan);
//bagian sorting
var ubah = lahir.sort(function (value1, value2) { return value2 - value1 } ) ;;
console.log(ubah)

//bagian join
var ambil = arr2[3].split("/");
console.log(ambil.join("-"));

//bagian slice
var nama = arr2[1].toString();

console.log(nama.slice(0,14));
