function sum(a,n,m){
	var sum = 0;
	var temp = 0;
	if(a>n){
		if (m == undefined) m=1;
		if (n == undefined) n=1;
		for(var i=a ; i>=n ; )
		{
			sum += i;
			i-=m;
		}
	}
	else {
		if (m == undefined) m=1;
		if (n == undefined) n=1;
		for(var i=a ; i<=n ; ){
			sum += i;
			i+=m;
		}
	}
	return sum;
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum())