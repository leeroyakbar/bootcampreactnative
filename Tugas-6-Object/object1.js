function arrayToObject(arr) {
	var firstName = arr[0][0]
	var lastName = arr[0][1]
	var gender = arr[0][2]
	var tahun = arr[0][3]

	var now = new Date()
	var thisyear = now.getFullYear()
	var age = thisyear - tahun

	var firstName2 = arr[1][0]
	var lastName2 = arr[1][1]
	var gender2 = arr[1][2]
	var tahun2 = arr[1][3]
	var age2
	if(tahun2 == undefined || tahun2 > thisyear){
		age2 = 'Invalid Birth Year'
	}
	else{
		age2 = thisyear - tahun2;
	}
	

    var arr2 = {
    	firstName1 : firstName,
    	lastName1 : lastName,
    	gender1 : gender,
    	age1 : age
    }
    console.log(arr2);

    var arr3 = {
    	firstName2 : firstName2,
    	lastName2 : lastName2,
    	gender2 : gender2,
    	age2 : age2
    }
    console.log(arr3);
   /* return arr2;
    return arr3;*/

}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
//arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
arrayToObject(people);
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/